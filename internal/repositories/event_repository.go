// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

type EventRepository interface {
	GetEvent(gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) DbEvent
	GetEventListByBsn(bsn systemModel.Bsn) []DbEvent
	AddDbEvent(event DbEvent) error
	AddDbBsnRelation(bsn systemModel.Bsn, gebeurteniskenmerk systemModel.GebeurtenisKenmerk) error
	AddDbFinancieleZaakRelation(gebeurteniskenmerk systemModel.GebeurtenisKenmerk, zaakkenmerk systemModel.Zaakkenmerk) error
	AddDbBetalingskenmerkGebeurtenis(betalingskenmerk string, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error
	GetDbFinancieleZaakRelationByGebeurtenisKenmerk(gebeurteniskenmerk systemModel.GebeurtenisKenmerk) ([]systemModel.Zaakkenmerk, error)
	GetDbGebeurtenisKenmerkRelationByBetalingskenmerk(betalingskenmerk string) (systemModel.GebeurtenisKenmerk, error)
	AddDbAchterstand(zaakkenmerk systemModel.Zaakkenmerk, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error
	GetDbAchterstanden(zaakkenmerk systemModel.Zaakkenmerk) ([]systemModel.GebeurtenisKenmerk, error)
	DeleteDbAchterstand(zaakkenmerk systemModel.Zaakkenmerk, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error
	AddDbContactOptie(contactOptie systemModel.ContactOptie, zaakkenmerk systemModel.Zaakkenmerk) error
	GetDbContactOpties(zaakkenmerk systemModel.Zaakkenmerk) ([]systemModel.ContactOptie, error)
	DeleteAll() error
}
