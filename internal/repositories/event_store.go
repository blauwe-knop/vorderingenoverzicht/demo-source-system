// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"
)

//Note: temporary solution to store events in memory
// when moving to a database we should deprecate the bsn in storage and use indexes

type EventStore struct {
	logger                               *zap.Logger
	events                               map[systemModel.GebeurtenisKenmerk]DbEvent
	bsnRelations                         map[systemModel.Bsn][]systemModel.GebeurtenisKenmerk
	financieleZaakRelations              map[systemModel.GebeurtenisKenmerk][]systemModel.Zaakkenmerk
	betalingskenmerkGebeurtenisRelations map[string]systemModel.GebeurtenisKenmerk
	achterstanden                        map[systemModel.Zaakkenmerk][]systemModel.GebeurtenisKenmerk
	contactOpties                        map[systemModel.Zaakkenmerk][]systemModel.ContactOptie
}

type DbEvent struct {
	Data []byte
	systemModel.BaseEvent
}

func NewEventStore(logger *zap.Logger) *EventStore {
	return &EventStore{
		logger:                               logger,
		events:                               map[systemModel.GebeurtenisKenmerk]DbEvent{},
		bsnRelations:                         map[systemModel.Bsn][]systemModel.GebeurtenisKenmerk{},
		financieleZaakRelations:              map[systemModel.GebeurtenisKenmerk][]systemModel.Zaakkenmerk{},
		betalingskenmerkGebeurtenisRelations: map[string]systemModel.GebeurtenisKenmerk{},
		achterstanden:                        map[systemModel.Zaakkenmerk][]systemModel.GebeurtenisKenmerk{},
		contactOpties:                        map[systemModel.Zaakkenmerk][]systemModel.ContactOptie{}}
}

func (s *EventStore) GetEvent(gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) DbEvent {
	s.logger.Debug("GetEvent")
	return s.events[gebeurtenisKenmerk]
}

func (s *EventStore) GetEventListByBsn(bsn systemModel.Bsn) []DbEvent {
	s.logger.Debug("GetEventListByBsn")
	dbEvents := []DbEvent{}

	gebeurtenisKenmerkenByBsn := s.bsnRelations[bsn]
	for _, gebeurtenisKenmerk := range gebeurtenisKenmerkenByBsn {
		dbEvents = append(dbEvents, s.events[gebeurtenisKenmerk])
	}
	return dbEvents
}

func (s *EventStore) AddDbEvent(event DbEvent) error {
	s.logger.Debug("AddDbEvent")
	s.events[event.GebeurtenisKenmerk] = event

	return nil
}

func (s *EventStore) AddDbBsnRelation(bsn systemModel.Bsn, gebeurteniskenmerk systemModel.GebeurtenisKenmerk) error {
	s.logger.Debug("AddDbBsnRelation")
	s.bsnRelations[bsn] = append(s.bsnRelations[bsn], gebeurteniskenmerk)

	return nil
}

func (s *EventStore) AddDbFinancieleZaakRelation(gebeurteniskenmerk systemModel.GebeurtenisKenmerk, zaakkenmerk systemModel.Zaakkenmerk) error {
	s.logger.Debug("AddDbFinancieleZaakRelation")
	s.financieleZaakRelations[gebeurteniskenmerk] = append(s.financieleZaakRelations[gebeurteniskenmerk], zaakkenmerk)

	return nil
}

func (s *EventStore) AddDbBetalingskenmerkGebeurtenis(betalingskenmerk string, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error {
	s.logger.Debug("AddDbBetalingskenmerkGebeurtenis")
	s.betalingskenmerkGebeurtenisRelations[betalingskenmerk] = gebeurtenisKenmerk

	return nil
}

func (s *EventStore) AddDbAchterstand(zaakkenmerk systemModel.Zaakkenmerk, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error {
	s.logger.Debug("AddDbAchterstand")
	s.achterstanden[zaakkenmerk] = append(s.achterstanden[zaakkenmerk], gebeurtenisKenmerk)

	return nil
}

func (s *EventStore) GetDbFinancieleZaakRelationByGebeurtenisKenmerk(gebeurteniskenmerk systemModel.GebeurtenisKenmerk) ([]systemModel.Zaakkenmerk, error) {
	s.logger.Debug("GetDbFinancieleZaakRelationByGebeurtenisKenmerk")
	return s.financieleZaakRelations[gebeurteniskenmerk], nil
}

func (s *EventStore) GetDbGebeurtenisKenmerkRelationByBetalingskenmerk(betalingskenmerk string) (systemModel.GebeurtenisKenmerk, error) {
	s.logger.Debug("GetDbFinancieleZaakRelationByBetalingskenmerk")
	return s.betalingskenmerkGebeurtenisRelations[betalingskenmerk], nil
}

func (s *EventStore) GetDbAchterstanden(zaakkenmerk systemModel.Zaakkenmerk) ([]systemModel.GebeurtenisKenmerk, error) {
	s.logger.Debug("GetDbAchterstanden")
	if s.achterstanden[zaakkenmerk] == nil {

		return []systemModel.GebeurtenisKenmerk{}, nil
	}
	return s.achterstanden[zaakkenmerk], nil
}

func (s *EventStore) DeleteDbAchterstand(zaakkenmerk systemModel.Zaakkenmerk, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error {
	s.logger.Debug("DeleteDbAchterstand")

	for i, kenmerk := range s.achterstanden[zaakkenmerk] {
		if kenmerk == gebeurtenisKenmerk {
			s.achterstanden[zaakkenmerk] = append(s.achterstanden[zaakkenmerk][:i], s.achterstanden[zaakkenmerk][i+1:]...)
			break
		}
	}

	return nil
}

func (s *EventStore) AddDbContactOptie(contactOptie systemModel.ContactOptie, zaakkenmerk systemModel.Zaakkenmerk) error {
	s.logger.Debug("AddDbContactOptie")
	s.contactOpties[zaakkenmerk] = append(s.contactOpties[zaakkenmerk], contactOptie)

	return nil
}

func (s *EventStore) GetDbContactOpties(zaakkenmerk systemModel.Zaakkenmerk) ([]systemModel.ContactOptie, error) {
	s.logger.Debug("GetDbContactOpties")
	if s.contactOpties[zaakkenmerk] == nil {

		return []systemModel.ContactOptie{}, nil
	}
	return s.contactOpties[zaakkenmerk], nil
}

func (s *EventStore) DeleteAll() error {
	s.events = map[systemModel.GebeurtenisKenmerk]DbEvent{}
	s.bsnRelations = map[systemModel.Bsn][]systemModel.GebeurtenisKenmerk{}
	s.financieleZaakRelations = map[systemModel.GebeurtenisKenmerk][]systemModel.Zaakkenmerk{}
	s.betalingskenmerkGebeurtenisRelations = map[string]systemModel.GebeurtenisKenmerk{}
	s.achterstanden = map[systemModel.Zaakkenmerk][]systemModel.GebeurtenisKenmerk{}
	s.contactOpties = map[systemModel.Zaakkenmerk][]systemModel.ContactOptie{}

	return nil
}
