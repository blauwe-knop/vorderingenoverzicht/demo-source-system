// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"io"
	"net/http"

	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/model"
)

func handlerGetFinancialClaimsInformationDocument(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	financialClaimInformationUseCase, _ := context.Value(financialClaimInformationUseCaseKey).(*usecases.FinancialClaimInformationUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.DESS_10
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var userIdentity systemModel.UserIdentity
	err := json.NewDecoder(request.Body).Decode(&userIdentity)
	if err != nil {
		event := events.DESS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	financialClaimsInformationDocument, err := financialClaimInformationUseCase.GetFinancialClaimsInformationDocument(userIdentity)
	if err != nil {
		event := events.DESS_19
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(responseWriter).Encode(financialClaimsInformationDocument)
	if err != nil {
		event := events.DESS_20
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.DESS_11
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerAddEvent(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	financialClaimInformationUseCase, _ := context.Value(financialClaimInformationUseCaseKey).(*usecases.FinancialClaimInformationUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	logEvent := events.DESS_12
	logger.Log(logEvent.GetLogLevel(), logEvent.Message, zap.Reflect("event", logEvent))

	eventJson, err := io.ReadAll(request.Body)
	if err != nil {
		event := events.DESS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	var event systemModel.Event

	var baseEvent systemModel.BaseEvent

	err = json.Unmarshal(eventJson, &baseEvent)
	if err != nil {
		event := events.DESS_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	switch baseEvent.GebeurtenisType {
	case "FinancieleVerplichtingOpgelegd":
		var financieleVerplichtingOpgelegdEvent systemModel.FinancieleVerplichtingOpgelegd
		err = json.Unmarshal(eventJson, &financieleVerplichtingOpgelegdEvent)

		event = financieleVerplichtingOpgelegdEvent

	case "FinancieleVerplichtingKwijtgescholden":
		var financieleVerplichtingKwijtgescholden systemModel.FinancieleVerplichtingKwijtgescholden
		err = json.Unmarshal(eventJson, &financieleVerplichtingKwijtgescholden)

		event = financieleVerplichtingKwijtgescholden

	case "FinancieleVerplichtingGecorrigeerd":
		var financieleVerplichtingGecorrigeerd systemModel.FinancieleVerplichtingGecorrigeerd
		err = json.Unmarshal(eventJson, &financieleVerplichtingGecorrigeerd)

		event = financieleVerplichtingGecorrigeerd

	case "BetalingsverplichtingOpgelegd":
		var betalingsverplichtingOpgelegd systemModel.BetalingsverplichtingOpgelegd
		err = json.Unmarshal(eventJson, &betalingsverplichtingOpgelegd)

		event = betalingsverplichtingOpgelegd
	case "BetalingsverplichtingIngetrokken":
		var betalingsverplichtingIngetrokken systemModel.BetalingsverplichtingIngetrokken
		err = json.Unmarshal(eventJson, &betalingsverplichtingIngetrokken)

		event = betalingsverplichtingIngetrokken
	case "BetalingVerwerkt":
		var betalingVerwerkt systemModel.BetalingVerwerkt
		err = json.Unmarshal(eventJson, &betalingVerwerkt)

		event = betalingVerwerkt

	case "FinancieelRechtVastgesteld":
		var financieelRechtVastgesteld systemModel.FinancieelRechtVastgesteld
		err = json.Unmarshal(eventJson, &financieelRechtVastgesteld)

		event = financieelRechtVastgesteld

	case "BedragUitbetaald":
		var bedragUitbetaald systemModel.BedragUitbetaald
		err = json.Unmarshal(eventJson, &bedragUitbetaald)

		event = bedragUitbetaald

	case "VerrekeningVerwerkt":
		var verrekeningVerwerkt systemModel.VerrekeningVerwerkt
		err = json.Unmarshal(eventJson, &verrekeningVerwerkt)

		event = verrekeningVerwerkt

	case "FinancieleZaakOvergedragen":
		var financieleZaakOvergedragen systemModel.FinancieleZaakOvergedragen
		err = json.Unmarshal(eventJson, &financieleZaakOvergedragen)

		event = financieleZaakOvergedragen

	case "FinancieleZaakOvergenomen":
		var financieleZaakOvergenomen systemModel.FinancieleZaakOvergenomen
		err = json.Unmarshal(eventJson, &financieleZaakOvergenomen)

		event = financieleZaakOvergenomen

	case "FinancieleZaakOvergedragenAanDeurwaarder":
		var financieleZaakOvergedragenAanDeurwaarder systemModel.FinancieleZaakOvergedragenAanDeurwaarder
		err = json.Unmarshal(eventJson, &financieleZaakOvergedragenAanDeurwaarder)

		event = financieleZaakOvergedragenAanDeurwaarder
	}
	if err != nil {
		event := events.DESS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = financialClaimInformationUseCase.AddEvent(event)
	if err != nil {
		event := events.DESS_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	logEvent = events.DESS_13
	logger.Log(logEvent.GetLogLevel(), logEvent.Message, zap.Reflect("event", logEvent))
}

func handlerAddAchterstand(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	financialClaimInformationUseCase, _ := context.Value(financialClaimInformationUseCaseKey).(*usecases.FinancialClaimInformationUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.DESS_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var achterstand model.CreateAchterstand
	err := json.NewDecoder(request.Body).Decode(&achterstand)
	if err != nil {
		event := events.DESS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = financialClaimInformationUseCase.AddAchterstand(achterstand.Zaakkenmerk, achterstand.GebeurtenisKenmerk)
	if err != nil {
		event := events.DESS_24
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.DESS_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerAddContactOpties(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	financialClaimInformationUseCase, _ := context.Value(financialClaimInformationUseCaseKey).(*usecases.FinancialClaimInformationUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.DESS_40
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var createContactOpties model.CreateContactOpties
	err := json.NewDecoder(request.Body).Decode(&createContactOpties)
	if err != nil {
		event := events.DESS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = financialClaimInformationUseCase.AddContactOpties(createContactOpties.ContactOpties, createContactOpties.Zaakkenmerk)
	if err != nil {
		event := events.DESS_42
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.DESS_41
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerReset(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	financialClaimInformationUseCase, _ := context.Value(financialClaimInformationUseCaseKey).(*usecases.FinancialClaimInformationUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.DESS_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	err := financialClaimInformationUseCase.DeleteAll()
	if err != nil {
		event := events.DESS_25
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")

	event = events.DESS_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
