// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/usecases"
)

type key int

const (
	financialClaimInformationUseCaseKey key = iota
	loggerKey                           key = iota
)

func NewRouter(financialClaimInformationUseCase *usecases.FinancialClaimInformationUseCase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})
	r.Use(cors.Handler)

	metricsMiddleware := metrics.NewMiddleware("demo-source-system")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v4", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "4.1.0"))

		r.Route("/financial_claims_information", func(r chi.Router) {
			r.Use(middleware.Logger)

			// Note: post instead of get as bsn is being received with this request.
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), financialClaimInformationUseCaseKey, financialClaimInformationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerGetFinancialClaimsInformationDocument(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/add_event", func(r chi.Router) {
			r.Use(middleware.Logger)

			// Note: post instead of get as bsn is being received with this request.
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), financialClaimInformationUseCaseKey, financialClaimInformationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerAddEvent(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/add_achterstand", func(r chi.Router) {
			r.Use(middleware.Logger)

			// Note: post instead of get as bsn is being received with this request.
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), financialClaimInformationUseCaseKey, financialClaimInformationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerAddAchterstand(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/add_contact_opties", func(r chi.Router) {
			r.Use(middleware.Logger)

			// Note: post instead of get as bsn is being received with this request.
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), financialClaimInformationUseCaseKey, financialClaimInformationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerAddContactOpties(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/reset", func(r chi.Router) {
			r.Use(middleware.Logger)

			r.Delete("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), financialClaimInformationUseCaseKey, financialClaimInformationUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerReset(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("demo-source-system", financialClaimInformationUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
