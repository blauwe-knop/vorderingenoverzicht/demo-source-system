// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/repositories"
	logEvents "gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/model"
)

type FinancialClaimInformationUseCase struct {
	organizationOin string
	eventRepository repositories.EventRepository
	Logger          *zap.Logger
}

func NewFinancialClaimInformationUseCase(logger *zap.Logger, organizationOin string, eventRepository repositories.EventRepository) *FinancialClaimInformationUseCase {
	return &FinancialClaimInformationUseCase{
		organizationOin: organizationOin,
		eventRepository: eventRepository,
		Logger:          logger,
	}
}

func (uc *FinancialClaimInformationUseCase) GetFinancialClaimsInformationDocument(userIdentity systemModel.UserIdentity) (*systemModel.FinancialClaimsInformationDocument, error) {
	events := uc.eventRepository.GetEventListByBsn(userIdentity.Bsn)

	eventsByZaakkenmerk := map[systemModel.Zaakkenmerk][]repositories.DbEvent{}

	for _, e := range events {
		zaakkenmerken, err := uc.eventRepository.GetDbFinancieleZaakRelationByGebeurtenisKenmerk(e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_26
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return nil, err
		}
		for _, z := range zaakkenmerken {
			eventsByZaakkenmerk[z] = append(eventsByZaakkenmerk[z], e)
		}
	}

	financieleZaken := []systemModel.FinancialClaimsInformationDocumentFinancieleZaak{}

	for zaakKenmerk, ebz := range eventsByZaakkenmerk {
		achterstanden, err := uc.eventRepository.GetDbAchterstanden(zaakKenmerk)
		if err != nil {
			event := logEvents.DESS_27
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return nil, err
		}
		contactOpties, err := uc.eventRepository.GetDbContactOpties(zaakKenmerk)
		if err != nil {
			event := logEvents.DESS_27
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return nil, err
		}

		financieleZaak, err := model.NewFinancieleZaakFromDBEvents(ebz, achterstanden, contactOpties)
		if err != nil {
			event := logEvents.DESS_28
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return nil, err
		}

		financieleZaken = append(financieleZaken, systemModel.FinancialClaimsInformationDocumentFinancieleZaak{
			Zaakkenmerk:               financieleZaak.Zaakkenmerk,
			Bsn:                       financieleZaak.Bsn,
			TotaalFinancieelVerplicht: financieleZaak.TotaalFinancieelVerplicht,
			TotaalFinancieelVereffend: financieleZaak.TotaalFinancieelVereffend,
			Saldo:                     financieleZaak.Saldo,
			SaldoDatumtijd:            financieleZaak.SaldoDatumtijd,
			Gebeurtenissen:            financieleZaak.Gebeurtenissen,
			Achterstanden:             financieleZaak.Achterstanden,
			ContactOpties:             financieleZaak.ContactOpties,
		})
	}

	//TODO: add actual beschikbaarheid
	beschikbaarheid := map[systemModel.FinancieleVerplichtingType]systemModel.BeschikbaarheidStatus{}

	financialClaimsInformationDocument := &systemModel.FinancialClaimsInformationDocument{
		Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V4",
		Version: "4",
		Body: systemModel.FinancialClaimsInformationDocumentBody{
			AangeleverdDoor:   uc.organizationOin,
			DocumentDatumtijd: time.Now(),
			Bsn:               userIdentity.Bsn,
			Beschikbaarheid:   beschikbaarheid,
			FinancieleZaken:   financieleZaken,
		},
	}

	return financialClaimsInformationDocument, nil
}

func (uc *FinancialClaimInformationUseCase) AddEvent(event systemModel.Event) error {
	uc.Logger.Debug("AddEvent")

	switch e := event.(type) {
	case systemModel.FinancieleVerplichtingOpgelegd:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieleVerplichtingKwijtgescholden:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieleVerplichtingGecorrigeerd:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.BetalingsverplichtingOpgelegd:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBetalingskenmerkGebeurtenis(e.Betalingskenmerk, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_33
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.BetalingsverplichtingIngetrokken:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		ingetrokkenDbEvent := uc.eventRepository.GetEvent(e.IngetrokkenGebeurtenisKenmerk)

		var ingetrokkenEvent systemModel.BetalingsverplichtingOpgelegd
		err = json.Unmarshal(ingetrokkenDbEvent.Data, &ingetrokkenEvent)
		if err != nil {
			event := logEvents.DESS_34
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
		err = uc.eventRepository.DeleteDbAchterstand(ingetrokkenEvent.Zaakkenmerk, ingetrokkenEvent.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_35
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(ingetrokkenEvent.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, ingetrokkenEvent.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.BetalingVerwerkt:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		gebeurtenisKenmerkBetalingsverplichtingOpgelegd, err := uc.eventRepository.GetDbGebeurtenisKenmerkRelationByBetalingskenmerk(e.Betalingskenmerk)
		if err != nil {
			event := logEvents.DESS_36
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		betalingsverplichtingOpgelegdDbEvent := uc.eventRepository.GetEvent(gebeurtenisKenmerkBetalingsverplichtingOpgelegd)

		var betalingsverplichtingOpgelegdEvent systemModel.BetalingsverplichtingOpgelegd
		err = json.Unmarshal(betalingsverplichtingOpgelegdDbEvent.Data, &betalingsverplichtingOpgelegdEvent)
		if err != nil {
			event := logEvents.DESS_37
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.DeleteDbAchterstand(betalingsverplichtingOpgelegdEvent.Zaakkenmerk, betalingsverplichtingOpgelegdEvent.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_35
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(betalingsverplichtingOpgelegdEvent.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, betalingsverplichtingOpgelegdEvent.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieelRechtVastgesteld:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.BedragUitbetaald:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.VerrekeningVerwerkt:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieleZaakOvergedragen:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieleZaakOvergenomen:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	case systemModel.FinancieleZaakOvergedragenAanDeurwaarder:
		eventJson, err := json.Marshal(e)
		if err != nil {
			event := logEvents.DESS_29
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		dbEvent := repositories.DbEvent{
			Data:      eventJson,
			BaseEvent: e.BaseEvent,
		}

		err = uc.eventRepository.AddDbEvent(dbEvent)
		if err != nil {
			event := logEvents.DESS_30
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbBsnRelation(e.Bsn, e.GebeurtenisKenmerk)
		if err != nil {
			event := logEvents.DESS_31
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}

		err = uc.eventRepository.AddDbFinancieleZaakRelation(e.GebeurtenisKenmerk, e.Zaakkenmerk)
		if err != nil {
			event := logEvents.DESS_32
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	}

	return nil
}

func (uc *FinancialClaimInformationUseCase) AddAchterstand(zaakKenmerk systemModel.Zaakkenmerk, gebeurtenisKenmerk systemModel.GebeurtenisKenmerk) error {
	uc.Logger.Debug("AddAchterstand")

	err := uc.eventRepository.AddDbAchterstand(zaakKenmerk, gebeurtenisKenmerk)
	if err != nil {
		event := logEvents.DESS_38
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return err
	}

	return nil
}

func (uc *FinancialClaimInformationUseCase) AddContactOpties(contactOpties []systemModel.ContactOptie, zaakKenmerk systemModel.Zaakkenmerk) error {
	uc.Logger.Debug("AddContactOpties")

	for _, contactOptie := range contactOpties {
		err := uc.eventRepository.AddDbContactOptie(contactOptie, zaakKenmerk)
		if err != nil {
			event := logEvents.DESS_43
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return err
		}
	}

	return nil
}

func (uc *FinancialClaimInformationUseCase) DeleteAll() error {
	err := uc.eventRepository.DeleteAll()
	if err != nil {
		event := logEvents.DESS_39
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return err
	}

	return nil
}

func (uc *FinancialClaimInformationUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{}
}
