package usecases

import (
	"encoding/json"
	"errors"
	"fmt"
	"maps"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/test/mock"
)

func TestFinancialClaimInformationUseCase_AddAchterstand(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	type args struct {
		zaakKenmerk        model.Zaakkenmerk
		gebeurtenisKenmerk model.GebeurtenisKenmerk
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedError error
	}{
		{
			name: "adding valid kenmerken results in achterstand being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddAchterstandEventRepositoryMock(t, "zaakkenmerk", "gebeurtenis-kenmerk"),
			},
			args: args{
				zaakKenmerk:        "zaakkenmerk",
				gebeurtenisKenmerk: "gebeurtenis-kenmerk",
			},
			expectedError: nil,
		},
		{
			name: "adding valid kenmerken with error storing to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddAchterstandEventRepositoryMockWithError(t, "zaakkenmerk", "gebeurtenis-kenmerk"),
			},
			args: args{
				zaakKenmerk:        "zaakkenmerk",
				gebeurtenisKenmerk: "gebeurtenis-kenmerk",
			},
			expectedError: errors.New("failed to store to DB"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}

			err := uc.AddAchterstand(tt.args.zaakKenmerk, tt.args.gebeurtenisKenmerk)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to store to DB", "AddAchterstand() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestFinancialClaimInformationUseCase_AddContactOptie(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	type args struct {
		contactOpties []model.ContactOptie
		zaakKenmerk   model.Zaakkenmerk
	}
	contactOptie := model.ContactOptie{
		ContactVorm:  model.ContactVormTelefoon,
		Url:          "tel:+318000543",
		Naam:         "Belastingtelefoon",
		Omschrijving: "Voor direct contact over uw zaak",
		Prioriteit:   1,
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedError error
	}{
		{
			name: "adding valid contact optie and zaakkenmerk results in achterstand being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddContactOptieEventRepositoryMock(t, contactOptie, "zaakkenmerk"),
			},
			args: args{
				contactOpties: []model.ContactOptie{contactOptie},
				zaakKenmerk:   "zaakkenmerk",
			},
			expectedError: nil,
		},
		{
			name: "adding valid contact optie and zaakkenmerk with error storing to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddContactOptieEventRepositoryMockWithError(t, contactOptie, "zaakkenmerk"),
			},
			args: args{
				contactOpties: []model.ContactOptie{contactOptie},
				zaakKenmerk:   "zaakkenmerk",
			},
			expectedError: errors.New("failed to store to DB"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}
			err := uc.AddContactOpties(tt.args.contactOpties, tt.args.zaakKenmerk)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to store to DB", "AddContactOptie() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestFinancialClaimInformationUseCase_AddEvent(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	type args struct {
		event model.Event
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedError error
	}{
		{
			name: "adding valid FinancieleVerplichtingOpgelegd event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleVerplichtingOpgelegd event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingOpgelegd event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingOpgelegd event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingKwijtgescholden event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleVerplichtingKwijtgescholden event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingKwijtgescholden event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingKwijtgescholden event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingGecorrigeerd event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleVerplichtingGecorrigeerd event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingGecorrigeerd event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleVerplichtingGecorrigeerd event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingOpgelegd event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid BetalingsverplichtingOpgelegd event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingOpgelegd event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingOpgelegd event with error storing betalingskenmerk gebeurtenis to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBetalingskenmerkGebeurtenis(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingOpgelegd event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingIngetrokken event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid BetalingsverplichtingIngetrokken event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingIngetrokken event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingIngetrokken event with error deleting achterstand from DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnDeleteDbAchterstand(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingsverplichtingIngetrokken event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingVerwerkt event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid BetalingVerwerkt event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingVerwerkt event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingVerwerkt event with error getting gebeurtenis kenmerk relation by betalingskenmerk from DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnGetDbGebeurtenisKenmerkRelationByBetalingskenmerk(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingVerwerkt event with error deleting achterstand from DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnDeleteDbAchterstand(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BetalingVerwerkt event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.BetalingVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieelRechtVastgesteld event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieelRechtVastgesteld event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieelRechtVastgesteld event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieelRechtVastgesteld event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BedragUitbetaald event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.BedragUitbetaald{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BedragUitbetaald{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid BedragUitbetaald event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.BedragUitbetaald{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BedragUitbetaald{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BedragUitbetaald event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.BedragUitbetaald{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BedragUitbetaald{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid BedragUitbetaald event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.BedragUitbetaald{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.BedragUitbetaald{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid VerrekeningVerwerkt event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid VerrekeningVerwerkt event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid VerrekeningVerwerkt event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid VerrekeningVerwerkt event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragen event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleZaakOvergedragen event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragen event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragen event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergenomen event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleZaakOvergenomen event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergenomen event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergenomen event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragenAanDeurwaarder event results in event being stored",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMock(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			},
			expectedError: nil,
		},
		{
			name: "adding valid FinancieleZaakOvergedragenAanDeurwaarder event with error storing event to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragenAanDeurwaarder event with error storing bsn relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
		{
			name: "adding valid FinancieleZaakOvergedragenAanDeurwaarder event with error storing financiele zaak relation to DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t, createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}))),
			},
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			},
			expectedError: errors.New("failed to store to DB"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}
			err := uc.AddEvent(tt.args.event)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to store to DB", "AddEvent() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestFinancialClaimInformationUseCase_DeleteAll(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	tests := []struct {
		name          string
		fields        fields
		expectedError error
	}{
		{
			name: "delete all results in everything being deleted",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createDeleteAllEventRepositoryMock(t),
			},
			expectedError: nil,
		},
		{
			name: "delete all with error deleting all from DB results in error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createDeleteAllEventRepositoryMockWithError(t),
			},
			expectedError: errors.New("failed to delete from DB"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}

			err := uc.DeleteAll()
			if err != nil {
				assert.EqualErrorf(t, err, "failed to delete from DB", "DeleteAll() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestFinancialClaimInformationUseCase_GetFinancialClaimsInformationDocument(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	type args struct {
		userIdentity model.UserIdentity
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		want          *model.FinancialClaimsInformationDocument
		expectedError error
	}{
		{
			name: "get financial claims information document results in returned valid document",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createGetFinancialClaimsInformationDocumentEventRepositoryMock(t),
			},
			args: args{
				userIdentity: model.UserIdentity{
					Bsn: model.Bsn("Bsn"),
				},
			},
			want:          createFinancialClaimsInformationDocument(),
			expectedError: nil,
		},
		{
			name: "get financial claims information document with error on getting financiele zaak relation from DB results in returned error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetFinancieleZaakRelation(t),
			},
			args: args{
				userIdentity: model.UserIdentity{
					Bsn: model.Bsn("Bsn"),
				},
			},
			want:          nil,
			expectedError: errors.New("failed to get DB financiele zaak relation"),
		},
		{
			name: "get financial claims information document with error on getting achterstanden from DB results in returned error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetAchterstanden(t),
			},
			args: args{
				userIdentity: model.UserIdentity{
					Bsn: model.Bsn("Bsn"),
				},
			},
			want:          nil,
			expectedError: errors.New("failed to get DB achterstanden"),
		},
		{
			name: "get financial claims information document with error on getting contact opties from DB results in returned error",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetDbContactOpties(t),
			},
			args: args{
				userIdentity: model.UserIdentity{
					Bsn: model.Bsn("Bsn"),
				},
			},
			want:          nil,
			expectedError: errors.New("failed to get DB contact opties"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}

			financialClaimsInformationDocument, err := uc.GetFinancialClaimsInformationDocument(tt.args.userIdentity)
			assert.Condition(t, func() bool {
				return assertFinancialClaimsInformationDocumentEqual(t, tt.want, financialClaimsInformationDocument)
			}, "The actual FinancialClaimsInformationDocument should be equal to expected. \nexpected: \"%v\"\nactual:   \"%v\"", tt.want, financialClaimsInformationDocument)
			if err != nil {
				assert.EqualErrorf(t, err, fmt.Sprint(tt.expectedError), "GetFinancialClaimsInformationDocument() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestFinancialClaimInformationUseCase_GetHealthChecks(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		organizationOin string
		eventRepository repositories.EventRepository
	}
	tests := []struct {
		name   string
		fields fields
		want   []healthcheck.Checker
	}{
		{
			name: "get health checks results in all health checks being returned",
			fields: fields{
				organizationOin: "00000006000000066000",
				eventRepository: nil,
			},
			want: []healthcheck.Checker{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &FinancialClaimInformationUseCase{
				organizationOin: tt.fields.organizationOin,
				eventRepository: tt.fields.eventRepository,
				Logger:          observedLogger,
			}

			healthChecks := uc.GetHealthChecks()
			assert.Equal(t, tt.want, healthChecks, fmt.Sprintf("GetHealthChecks() healthChecks = %v, want = %v", healthChecks, tt.want))
		})
	}
}

func TestNewFinancialClaimInformationUseCase(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type args struct {
		logger          *zap.Logger
		organizationOin string
		eventRepository repositories.EventRepository
	}
	tests := []struct {
		name string
		args args
		want *FinancialClaimInformationUseCase
	}{
		{
			name: "get health checks results in all health checks being returned",
			args: args{
				logger:          observedLogger,
				organizationOin: "00000006000000066000",
				eventRepository: nil,
			},
			want: &FinancialClaimInformationUseCase{
				organizationOin: "00000006000000066000",
				eventRepository: nil,
				Logger:          observedLogger,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			usecase := NewFinancialClaimInformationUseCase(
				tt.args.logger,
				tt.args.organizationOin,
				tt.args.eventRepository,
			)
			assert.Equal(t, tt.want, usecase, fmt.Sprintf("NewFinancialClaimInformationUseCase() usecase = %v, want = %v", usecase, tt.want))
		})
	}
}

func createAddAchterstandEventRepositoryMock(t *testing.T, expectedZaakkenmerk model.Zaakkenmerk, expectedGebeurtenisKenmerk model.GebeurtenisKenmerk) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbAchterstand(expectedZaakkenmerk, expectedGebeurtenisKenmerk).
		Return(nil)
	return eventRepository
}

func createAddAchterstandEventRepositoryMockWithError(t *testing.T, expectedZaakkenmerk model.Zaakkenmerk, expectedGebeurtenisKenmerk model.GebeurtenisKenmerk) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbAchterstand(expectedZaakkenmerk, expectedGebeurtenisKenmerk).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createAddContactOptieEventRepositoryMock(t *testing.T, expectedContactOptie model.ContactOptie, expectedZaakkenmerk model.Zaakkenmerk) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbContactOptie(expectedContactOptie, expectedZaakkenmerk).
		Return(nil)
	return eventRepository
}

func createAddContactOptieEventRepositoryMockWithError(t *testing.T, expectedContactOptie model.ContactOptie, expectedZaakkenmerk model.Zaakkenmerk) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbContactOptie(expectedContactOptie, expectedZaakkenmerk).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createAddEventEventRepositoryMock(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)
	eventRepository.
		EXPECT().
		AddDbBsnRelation(model.Bsn("Bsn"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingsverplichtingOpgelegd:
		eventRepository.
			EXPECT().
			AddDbBetalingskenmerkGebeurtenis("Betalingskenmerk", model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingsverplichtingIngetrokken:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("IngetrokkenGebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingVerwerkt:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), nil)
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	}

	eventRepository.
		EXPECT().
		AddDbFinancieleZaakRelation(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), model.Zaakkenmerk("Zaakkenmerk")).
		Return(nil)

	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnAddDbEvent(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnAddDbBsnRelation(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingsverplichtingIngetrokken:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("IngetrokkenGebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingVerwerkt:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), nil)
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	}

	eventRepository.
		EXPECT().
		AddDbBsnRelation(model.Bsn("Bsn"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnAddDbBetalingskenmerkGebeurtenis(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)
	eventRepository.
		EXPECT().
		AddDbBsnRelation(model.Bsn("Bsn"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingsverplichtingIngetrokken:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("IngetrokkenGebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingVerwerkt:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(nil)
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	}

	eventRepository.
		EXPECT().
		AddDbBetalingskenmerkGebeurtenis("Betalingskenmerk", model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnGetDbGebeurtenisKenmerkRelationByBetalingskenmerk(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingVerwerkt:
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(model.GebeurtenisKenmerk(""), errors.New("failed to store to DB"))
	}
	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnDeleteDbAchterstand(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingsverplichtingIngetrokken:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("IngetrokkenGebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(errors.New("failed to store to DB"))
	case model.BetalingVerwerkt:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), nil)
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(errors.New("failed to store to DB"))
	}
	return eventRepository
}

func createAddEventEventRepositoryMockWithErrorOnAddDbFinancieleZaakRelation(t *testing.T, expectedEvent model.Event) repositories.EventRepository {
	controller := gomock.NewController(t)

	expectedEventJson, _ := json.Marshal(expectedEvent)
	baseEvent := getBaseEvent(expectedEvent)
	dbEvent := repositories.DbEvent{
		Data:      expectedEventJson,
		BaseEvent: baseEvent,
	}

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		AddDbEvent(dbEvent).
		Return(nil)
	eventRepository.
		EXPECT().
		AddDbBsnRelation(model.Bsn("Bsn"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(nil)

	switch expectedEvent.(type) {
	case model.BetalingsverplichtingOpgelegd:
		eventRepository.
			EXPECT().
			AddDbBetalingskenmerkGebeurtenis("Betalingskenmerk", model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingsverplichtingIngetrokken:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("IngetrokkenGebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	case model.BetalingVerwerkt:
		betalingsverplichtingOpgelegdEvent := createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}))
		betalingsverplichtingOpgelegdEventJson, _ := json.Marshal(betalingsverplichtingOpgelegdEvent)
		var betalingsverplichtingOpgelegdDbEvent repositories.DbEvent
		switch e := betalingsverplichtingOpgelegdEvent.(type) {
		case model.BetalingsverplichtingOpgelegd:
			betalingsverplichtingOpgelegdDbEvent = repositories.DbEvent{
				Data:      betalingsverplichtingOpgelegdEventJson,
				BaseEvent: e.BaseEvent,
			}
		}
		eventRepository.
			EXPECT().
			GetDbGebeurtenisKenmerkRelationByBetalingskenmerk("Betalingskenmerk").
			Return(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), nil)
		eventRepository.
			EXPECT().
			GetEvent(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(betalingsverplichtingOpgelegdDbEvent)
		eventRepository.
			EXPECT().
			DeleteDbAchterstand(model.Zaakkenmerk("Zaakkenmerk"), model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
			Return(nil)
	}

	eventRepository.
		EXPECT().
		AddDbFinancieleZaakRelation(model.GebeurtenisKenmerk("GebeurtenisKenmerk"), model.Zaakkenmerk("Zaakkenmerk")).
		Return(errors.New("failed to store to DB"))
	return eventRepository
}

func createDeleteAllEventRepositoryMock(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		DeleteAll().
		Return(nil)
	return eventRepository
}

func createDeleteAllEventRepositoryMockWithError(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		DeleteAll().
		Return(errors.New("failed to delete from DB"))
	return eventRepository
}

func createGetFinancialClaimsInformationDocumentEventRepositoryMock(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	events := getEvents()
	eventJson1, _ := json.Marshal(events[0])
	eventJson2, _ := json.Marshal(events[1])

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		GetEventListByBsn(model.Bsn("Bsn")).
		Return([]repositories.DbEvent{
			{
				Data: eventJson1,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
			{
				Data: eventJson2,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "BetalingsverplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk2",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
		})

	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)
	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk2")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)

	eventRepository.
		EXPECT().
		GetDbAchterstanden(model.Zaakkenmerk("Zaakkenmerk")).
		Return([]model.GebeurtenisKenmerk{
			"FinancieleVerplichtingOpgelegd",
			"GebeurtenisKenmerk",
		}, nil)

	eventRepository.
		EXPECT().
		GetDbContactOpties(model.Zaakkenmerk("Zaakkenmerk")).
		Return(getContactOpties(), nil)
	return eventRepository
}

func createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetFinancieleZaakRelation(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	events := getEvents()
	eventJson1, _ := json.Marshal(events[0])
	eventJson2, _ := json.Marshal(events[1])

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		GetEventListByBsn(model.Bsn("Bsn")).
		Return([]repositories.DbEvent{
			{
				Data: eventJson1,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
			{
				Data: eventJson2,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "BetalingsverplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk2",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
		})

	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return(nil, errors.New("failed to get DB financiele zaak relation"))
	return eventRepository
}

func createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetAchterstanden(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	events := getEvents()
	eventJson1, _ := json.Marshal(events[0])
	eventJson2, _ := json.Marshal(events[1])

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		GetEventListByBsn(model.Bsn("Bsn")).
		Return([]repositories.DbEvent{
			{
				Data: eventJson1,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
			{
				Data: eventJson2,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "BetalingsverplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk2",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
		})

	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)
	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk2")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)

	eventRepository.
		EXPECT().
		GetDbAchterstanden(model.Zaakkenmerk("Zaakkenmerk")).
		Return(nil, errors.New("failed to get DB achterstanden"))
	return eventRepository
}

func createGetFinancialClaimsInformationDocumentEventRepositoryMockWithErrorOnGetDbContactOpties(t *testing.T) repositories.EventRepository {
	controller := gomock.NewController(t)

	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	events := getEvents()
	eventJson1, _ := json.Marshal(events[0])
	eventJson2, _ := json.Marshal(events[1])

	eventRepository := mock.NewMockEventRepository(controller)
	eventRepository.
		EXPECT().
		GetEventListByBsn(model.Bsn("Bsn")).
		Return([]repositories.DbEvent{
			{
				Data: eventJson1,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
			{
				Data: eventJson2,
				BaseEvent: model.BaseEvent{
					GebeurtenisType:      "BetalingsverplichtingOpgelegd",
					GebeurtenisKenmerk:   "GebeurtenisKenmerk2",
					DatumtijdGebeurtenis: datumtijd,
				},
			},
		})

	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)
	eventRepository.
		EXPECT().
		GetDbFinancieleZaakRelationByGebeurtenisKenmerk(model.GebeurtenisKenmerk("GebeurtenisKenmerk2")).
		Return([]model.Zaakkenmerk{
			"Zaakkenmerk",
		}, nil)

	eventRepository.
		EXPECT().
		GetDbAchterstanden(model.Zaakkenmerk("Zaakkenmerk")).
		Return([]model.GebeurtenisKenmerk{
			"GebeurtenisKenmerk",
			"GebeurtenisKenmerk2",
		}, nil)

	eventRepository.
		EXPECT().
		GetDbContactOpties(model.Zaakkenmerk("Zaakkenmerk")).
		Return(nil, errors.New("failed to get DB contact opties"))
	return eventRepository
}

func getBaseEvent(event model.Event) model.BaseEvent {
	switch e := event.(type) {
	case model.FinancieleVerplichtingOpgelegd:
		return e.BaseEvent
	case model.FinancieleVerplichtingKwijtgescholden:
		return e.BaseEvent
	case model.FinancieleVerplichtingGecorrigeerd:
		return e.BaseEvent
	case model.FinancieelRechtVastgesteld:
		return e.BaseEvent
	case model.BetalingsverplichtingIngetrokken:
		return e.BaseEvent
	case model.BetalingsverplichtingOpgelegd:
		return e.BaseEvent
	case model.BetalingVerwerkt:
		return e.BaseEvent
	case model.BedragUitbetaald:
		return e.BaseEvent
	case model.VerrekeningVerwerkt:
		return e.BaseEvent
	case model.FinancieleZaakOvergedragen:
		return e.BaseEvent
	case model.FinancieleZaakOvergenomen:
		return e.BaseEvent
	case model.FinancieleZaakOvergedragenAanDeurwaarder:
		return e.BaseEvent
	}
	return model.BaseEvent{}
}

func createEvent(eventType reflect.Type) model.Event {
	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	switch eventType.Name() {
	case reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}).Name():
		return model.FinancieleVerplichtingOpgelegd{
			DatumtijdOpgelegd:               datumtijd,
			Zaakkenmerk:                     "Zaakkenmerk",
			Beschikkingsnummer:              "Beschikkingsnummer",
			Bsn:                             "Bsn",
			PrimaireVerplichting:            false,
			Type:                            "Type",
			Categorie:                       "Categorie",
			Bedrag:                          50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			OpgelegdDoor:                    "OpgelegdDoor",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}).Name():
		return model.FinancieleVerplichtingKwijtgescholden{
			KenmerkKwijtgescholdenFinancieleVerplichting: "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			BedragKwijtschelding:            50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingKwijtgescholden",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}).Name():
		return model.FinancieleVerplichtingGecorrigeerd{
			KenmerkGecorrigeerdeFinancieleVerplichting: "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			NieuwOpgelegdBedrag:             60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingGecorrigeerd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieelRechtVastgesteld{}).Name():
		return model.FinancieelRechtVastgesteld{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			Bedrag:                          50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieelRechtVastgesteld",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}).Name():
		return model.BetalingsverplichtingIngetrokken{
			IngetrokkenGebeurtenisKenmerk: "IngetrokkenGebeurtenisKenmerk",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingsverplichtingIngetrokken",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}).Name():
		return model.BetalingsverplichtingOpgelegd{
			DatumtijdOpgelegd:            datumtijd,
			Zaakkenmerk:                  "Zaakkenmerk",
			Bsn:                          "Bsn",
			Bedrag:                       50,
			Omschrijving:                 "Omschrijving",
			Type:                         "Type",
			Betaalwijze:                  "Betaalwijze",
			TeBetalenAan:                 "TeBetalenAan",
			Rekeningnummer:               "Rekeningnummer",
			RekeningnummerTenaamstelling: "RekeningnummerTenaamstelling",
			Betalingskenmerk:             "Betalingskenmerk",
			Vervaldatum:                  datumtijd,
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingsverplichtingOpgelegd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingVerwerkt{}).Name():
		return model.BetalingVerwerkt{
			DatumtijdVerwerkt:  datumtijd,
			Betalingskenmerk:   "Betalingskenmerk",
			DatumtijdOntvangen: datumtijd,
			OntvangenDoor:      "OntvangenDoor",
			VerwerktDoor:       "VerwerktDoor",
			Bedrag:             60,
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingVerwerkt",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BedragUitbetaald{}).Name():
		return model.BedragUitbetaald{
			Zaakkenmerk:    "Zaakkenmerk",
			Bsn:            "Bsn",
			Bedrag:         50,
			Rekeningnummer: "Rekeningnummer",
			Omschrijving:   "Omschrijving",
			UitgevoerdDoor: "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BedragUitbetaald",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.VerrekeningVerwerkt{}).Name():
		return model.VerrekeningVerwerkt{
			KenmerkFinancieleVerplichting:   "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			BedragVerrekening:               40,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "VerrekeningVerwerkt",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergedragen{}).Name():
		return model.FinancieleZaakOvergedragen{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergedragenAan:                 "OvergedragenAan",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergedragen",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergenomen{}).Name():
		return model.FinancieleZaakOvergenomen{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergenomenVan:                  "OvergenomenVan",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergenomen",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}).Name():
		return model.FinancieleZaakOvergedragenAanDeurwaarder{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergedragenAan:                 "OvergedragenAan",
			NaamDeurwaarder:                 "NaamDeurwaarder",
			TelefoonnummerDeurwaarderUrl:    "TelefoonnummerDeurwaarderUrl",
			EmailAdresDeurwaarderUrl:        "EmailAdresDeurwaarderUrl",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergedragenAanDeurwaarder",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}
	}
	return nil
}

func createFinancialClaimsInformationDocument() *model.FinancialClaimsInformationDocument {
	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	events := getEvents()

	return &model.FinancialClaimsInformationDocument{
		Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V4",
		Version: "4",
		Body: model.FinancialClaimsInformationDocumentBody{
			AangeleverdDoor:   "00000006000000066000",
			DocumentDatumtijd: datumtijd,
			Bsn:               "Bsn",
			Beschikbaarheid:   map[model.FinancieleVerplichtingType]model.BeschikbaarheidStatus{},
			FinancieleZaken: []model.FinancialClaimsInformationDocumentFinancieleZaak{
				{
					Zaakkenmerk:               "Zaakkenmerk",
					Bsn:                       "Bsn",
					TotaalFinancieelVerplicht: 40,
					TotaalFinancieelVereffend: 0,
					Saldo:                     40,
					SaldoDatumtijd:            datumtijd,
					Gebeurtenissen: []interface{}{
						events[0],
						events[1],
					},
					Achterstanden: []model.GebeurtenisKenmerk{
						"FinancieleVerplichtingOpgelegd",
						"GebeurtenisKenmerk",
					},
					ContactOpties: getContactOpties(),
				},
			},
		},
	}
}

func getContactOpties() []model.ContactOptie {
	return []model.ContactOptie{
		{
			ContactVorm:  model.ContactVormTelefoon,
			Url:          "tel:+318000543",
			Naam:         "Belastingtelefoon",
			Omschrijving: "Voor direct contact over uw zaak",
			Prioriteit:   1,
		},
		{
			ContactVorm:  model.ContactVormEmail,
			Url:          "info@belastingdienst.nl",
			Naam:         "Belastingmail",
			Omschrijving: "Voor direct contact over uw zaak",
			Prioriteit:   2,
		},
	}
}

func getEvents() []model.Event {
	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	event1 := model.FinancieleVerplichtingOpgelegd{
		DatumtijdOpgelegd:               datumtijd,
		Zaakkenmerk:                     "Zaakkenmerk",
		Beschikkingsnummer:              "Beschikkingsnummer",
		Bsn:                             "Bsn",
		PrimaireVerplichting:            true,
		Type:                            "Type",
		Categorie:                       "Categorie",
		Bedrag:                          40,
		Omschrijving:                    "Omschrijving",
		JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
		JuridischeGrondslagBron:         "JuridischeGrondslagBron",
		OpgelegdDoor:                    "OpgelegdDoor",
		UitgevoerdDoor:                  "UitgevoerdDoor",
		BaseEvent: model.BaseEvent{
			GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
			GebeurtenisKenmerk:   "GebeurtenisKenmerk",
			DatumtijdGebeurtenis: datumtijd,
		},
	}
	event2 := model.BetalingsverplichtingOpgelegd{
		DatumtijdOpgelegd:            datumtijd,
		Zaakkenmerk:                  "Zaakkenmerk",
		Bsn:                          "Bsn",
		Bedrag:                       30,
		Omschrijving:                 "Omschrijving",
		Type:                         "Type",
		Betaalwijze:                  "Betaalwijze",
		TeBetalenAan:                 "TeBetalenAan",
		Rekeningnummer:               "Rekeningnummer",
		RekeningnummerTenaamstelling: "RekeningnummerTenaamstelling",
		Betalingskenmerk:             "Betalingskenmerk",
		Vervaldatum:                  datumtijd,
		BaseEvent: model.BaseEvent{
			GebeurtenisType:      "BetalingsverplichtingOpgelegd",
			GebeurtenisKenmerk:   "GebeurtenisKenmerk2",
			DatumtijdGebeurtenis: datumtijd,
		},
	}
	return []model.Event{
		event1,
		event2,
	}
}

func assertFinancialClaimsInformationDocumentEqual(t testing.TB, expected, actual *model.FinancialClaimsInformationDocument) bool {
	if expected == nil && actual == nil {
		return true
	} else if expected == nil || actual == nil {
		return false
	}

	if actual.Type != expected.Type ||
		actual.Version != expected.Version ||
		actual.Body.Bsn != expected.Body.Bsn ||
		actual.Body.AangeleverdDoor != expected.Body.AangeleverdDoor ||
		!maps.Equal(actual.Body.Beschikbaarheid, expected.Body.Beschikbaarheid) {
		return false
	}

	for i, actualFinancieleZaak := range actual.Body.FinancieleZaken {
		expectedFinancieleZaak := expected.Body.FinancieleZaken[i]
		if actualFinancieleZaak.Bsn != expectedFinancieleZaak.Bsn ||
			actualFinancieleZaak.Zaakkenmerk != expectedFinancieleZaak.Zaakkenmerk ||
			actualFinancieleZaak.Saldo != expectedFinancieleZaak.Saldo ||
			actualFinancieleZaak.TotaalFinancieelVerplicht != expectedFinancieleZaak.TotaalFinancieelVerplicht ||
			actualFinancieleZaak.TotaalFinancieelVereffend != expectedFinancieleZaak.TotaalFinancieelVereffend {
			return false
		}
		for ig, actualGebeurtenis := range actualFinancieleZaak.Gebeurtenissen {
			expectedGebeurtenis := expectedFinancieleZaak.Gebeurtenissen[ig]
			switch ag := actualGebeurtenis.(type) {
			case model.FinancieleVerplichtingOpgelegd:
				switch eg := expectedGebeurtenis.(type) {
				case model.FinancieleVerplichtingOpgelegd:
					if ag.Bsn != eg.Bsn ||
						ag.Zaakkenmerk != eg.Zaakkenmerk ||
						ag.Type != eg.Type ||
						ag.UitgevoerdDoor != eg.UitgevoerdDoor ||
						ag.JuridischeGrondslagOmschrijving != eg.JuridischeGrondslagOmschrijving ||
						ag.JuridischeGrondslagBron != eg.JuridischeGrondslagBron ||
						ag.Omschrijving != eg.Omschrijving ||
						ag.Beschikkingsnummer != eg.Beschikkingsnummer ||
						ag.Categorie != eg.Categorie ||
						ag.GebeurtenisType != eg.GebeurtenisType ||
						ag.GebeurtenisKenmerk != eg.GebeurtenisKenmerk ||
						ag.OpgelegdDoor != eg.OpgelegdDoor ||
						ag.Bedrag != eg.Bedrag ||
						ag.PrimaireVerplichting != eg.PrimaireVerplichting {
						return false
					}
				default:
					return false
				}
			case model.BetalingsverplichtingOpgelegd:
				switch eg := expectedGebeurtenis.(type) {
				case model.BetalingsverplichtingOpgelegd:
					if ag.Bsn != eg.Bsn ||
						ag.Zaakkenmerk != eg.Zaakkenmerk ||
						ag.Type != eg.Type ||
						ag.Omschrijving != eg.Omschrijving ||
						ag.GebeurtenisType != eg.GebeurtenisType ||
						ag.GebeurtenisKenmerk != eg.GebeurtenisKenmerk ||
						ag.Betalingskenmerk != eg.Betalingskenmerk ||
						ag.Betaalwijze != eg.Betaalwijze ||
						ag.RekeningnummerTenaamstelling != eg.RekeningnummerTenaamstelling ||
						ag.Rekeningnummer != eg.Rekeningnummer ||
						ag.TeBetalenAan != eg.TeBetalenAan ||
						ag.Bedrag != eg.Bedrag {
						return false
					}
				default:
					return false
				}
			}
		}
		for ia, actualAchterstand := range actualFinancieleZaak.Achterstanden {
			expectedAchterstand := expectedFinancieleZaak.Achterstanden[ia]
			if actualAchterstand != expectedAchterstand {
				return false
			}
		}
		for ic, actualContactOpties := range actualFinancieleZaak.ContactOpties {
			expectedContactOpties := expectedFinancieleZaak.ContactOpties[ic]
			if actualContactOpties != expectedContactOpties {
				return false
			}
		}
	}

	return true
}
