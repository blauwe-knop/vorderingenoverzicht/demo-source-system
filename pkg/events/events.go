package events

var (
	DESS_1 = NewEvent(
		"dess_1",
		"started listening",
		Low,
	)
	DESS_2 = NewEvent(
		"dess_2",
		"server closed",
		High,
	)
	DESS_3 = NewEvent(
		"dess_3",
		"received request for openapi spec as JSON",
		Low,
	)
	DESS_4 = NewEvent(
		"dess_4",
		"sent response with openapi spec as JSON",
		Low,
	)
	DESS_5 = NewEvent(
		"dess_5",
		"received request for openapi spec as YAML",
		Low,
	)
	DESS_6 = NewEvent(
		"dess_6",
		"sent response with openapi spec as YAML",
		Low,
	)
	DESS_7 = NewEvent(
		"dess_7",
		"failed to read openapi.json file",
		High,
	)
	DESS_8 = NewEvent(
		"dess_8",
		"failed to write fileBytes to response",
		High,
	)
	DESS_9 = NewEvent(
		"dess_9",
		"failed to read openapi.yaml file",
		High,
	)
	DESS_10 = NewEvent(
		"dess_10",
		"received request for financial claims information document",
		Low,
	)
	DESS_11 = NewEvent(
		"dess_11",
		"sent response with financial claims information document",
		Low,
	)
	DESS_12 = NewEvent(
		"dess_12",
		"received request to add event",
		Low,
	)
	DESS_13 = NewEvent(
		"dess_13",
		"added new session",
		Low,
	)
	DESS_14 = NewEvent(
		"dess_14",
		"received request to add achterstand",
		Low,
	)
	DESS_15 = NewEvent(
		"dess_15",
		"added new achterstand",
		Low,
	)
	DESS_16 = NewEvent(
		"dess_16",
		"received request to delete all financial claims information documents",
		Low,
	)
	DESS_17 = NewEvent(
		"dess_17",
		"deleted all financial claims information documents",
		Low,
	)
	DESS_18 = NewEvent(
		"dess_18",
		"failed to decode request payload",
		High,
	)
	DESS_19 = NewEvent(
		"dess_19",
		"error retrieving financial claims information document",
		High,
	)
	DESS_20 = NewEvent(
		"dess_20",
		"failed to encode response payload",
		High,
	)
	DESS_21 = NewEvent(
		"dess_21",
		"failed to decode request payload bytes",
		High,
	)
	DESS_22 = NewEvent(
		"dess_22",
		"failed to decode request payload baseEvent",
		High,
	)
	DESS_23 = NewEvent(
		"dess_23",
		"error adding event",
		High,
	)
	DESS_24 = NewEvent(
		"dess_24",
		"failed to add achterstand",
		High,
	)
	DESS_25 = NewEvent(
		"dess_25",
		"error deleting all financial claims information documents",
		High,
	)
	DESS_26 = NewEvent(
		"dess_26",
		"failed to get FinancieleZaakRelationByGebeurtenisKenmerk",
		High,
	)
	DESS_27 = NewEvent(
		"dess_27",
		"failed to get Achterstanden",
		High,
	)
	DESS_28 = NewEvent(
		"dess_28",
		"failed to create events from Achterstanden",
		High,
	)
	DESS_29 = NewEvent(
		"dess_29",
		"failed to encode event to json",
		High,
	)
	DESS_30 = NewEvent(
		"dess_30",
		"failed to add Event to db",
		High,
	)
	DESS_31 = NewEvent(
		"dess_31",
		"failed to add BsnRelation to db",
		High,
	)
	DESS_32 = NewEvent(
		"dess_32",
		"failed to add FinancieleZaakRelation to db",
		High,
	)
	DESS_33 = NewEvent(
		"dess_33",
		"failed to add BetalingskenmerkGebeurtenis to db",
		High,
	)
	DESS_34 = NewEvent(
		"dess_34",
		"failed to decode ingetrokkenEvent from db event",
		High,
	)
	DESS_35 = NewEvent(
		"dess_35",
		"failed to delete Achterstand from db",
		High,
	)
	DESS_36 = NewEvent(
		"dess_36",
		"failed to get GebeurtenisKenmerkRelationByBetalingskenmerk from db",
		High,
	)
	DESS_37 = NewEvent(
		"dess_37",
		"failed to decode betalingsverplichtingOpgelegdEvent from db event",
		High,
	)
	DESS_38 = NewEvent(
		"dess_38",
		"failed to add Achterstand to db",
		High,
	)
	DESS_39 = NewEvent(
		"dess_39",
		"failed to delete all events from db",
		High,
	)
	DESS_40 = NewEvent(
		"dess_40",
		"received request to add contact optie",
		Low,
	)
	DESS_41 = NewEvent(
		"dess_41",
		"added new contact opties",
		Low,
	)
	DESS_42 = NewEvent(
		"dess_42",
		"failed to add contact optie",
		High,
	)
	DESS_43 = NewEvent(
		"dess_43",
		"failed to add ContactOptie to db",
		High,
	)
)
