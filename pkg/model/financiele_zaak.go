// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"encoding/json"

	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/repositories"
)

func NewFinancieleZaakFromDBEvents(dbEvents []repositories.DbEvent, achterstanden []systemModel.GebeurtenisKenmerk, contactOpties []systemModel.ContactOptie) (*systemModel.FinancialClaimsInformationDocumentFinancieleZaak, error) {
	var events []systemModel.Event

	for _, dbEvent := range dbEvents {
		switch dbEvent.BaseEvent.GebeurtenisType {
		case "FinancieleVerplichtingOpgelegd":
			var event systemModel.FinancieleVerplichtingOpgelegd
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieleVerplichtingKwijtgescholden":
			var event systemModel.FinancieleVerplichtingKwijtgescholden
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieleVerplichtingGecorrigeerd":
			var event systemModel.FinancieleVerplichtingGecorrigeerd
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "BetalingsverplichtingOpgelegd":
			var event systemModel.BetalingsverplichtingOpgelegd
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}
			events = append(events, event)

		case "BetalingsverplichtingIngetrokken":
			var event systemModel.BetalingsverplichtingIngetrokken
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "BetalingVerwerkt":
			var event systemModel.BetalingVerwerkt
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieelRechtVastgesteld":
			var event systemModel.FinancieelRechtVastgesteld
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "BedragUitbetaald":
			var event systemModel.BedragUitbetaald
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "VerrekeningVerwerkt":
			var event systemModel.VerrekeningVerwerkt
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieleZaakOvergedragen":
			var event systemModel.FinancieleZaakOvergedragen
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieleZaakOvergenomen":
			var event systemModel.FinancieleZaakOvergenomen
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)

		case "FinancieleZaakOvergedragenAanDeurwaarder":
			var event systemModel.FinancieleZaakOvergedragenAanDeurwaarder
			err := json.Unmarshal(dbEvent.Data, &event)
			if err != nil {
				return nil, err
			}

			events = append(events, event)
		}
	}

	financieleZaak, err := systemModel.NewFinancieleZaakFromEvents(events, achterstanden, contactOpties)
	if err != nil {
		return nil, err
	}

	return financieleZaak, nil
}
