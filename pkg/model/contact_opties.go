package model

import systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

type CreateContactOpties struct {
	ContactOpties []systemModel.ContactOptie `json:"contact_opties"`
	Zaakkenmerk   systemModel.Zaakkenmerk    `json:"zaakkenmerk"`
}
