package model_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/model"
)

func TestNewFinancieleZaakFromDBEvents(t *testing.T) {
	dbEvents := []repositories.DbEvent{
		{
			BaseEvent: systemModel.BaseEvent{
				GebeurtenisType: "FinancieleVerplichtingOpgelegd",
			},
			Data: json.RawMessage(`{
  "gebeurtenis_type": "FinancieleVerplichtingOpgelegd",
  "gebeurtenis_kenmerk": "81029991234567891-1",
  "datumtijd_gebeurtenis": "2023-03-31T10:25:00+00:00",
  "datumtijd_opgelegd": "2023-03-31T10:25:00+00:00",
  "zaakkenmerk": "81029991234567891",
  "bsn": "814859094",
  "primaire_verplichting": true,
  "type": "CJIB_WAHV",
  "categorie": "Algemeen",
  "bedrag": 10400,
  "omschrijving": "Kenteken: a-000-aa; 11 km te hard buiten de bebouwde kom op 31 maart 2023",
  "juridische_grondslag_omschrijving": "Boete voor te hard rijden Artikel 21 (RVV 1990)",
  "juridische_grondslag_bron": "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
  "opgelegd_door": "00000004000000044000",
  "uitgevoerd_door": "00000004000000044000"
}
`),
		},
		{
			BaseEvent: systemModel.BaseEvent{
				GebeurtenisType: "BetalingVerwerkt",
			},
			Data: json.RawMessage(`{
  "gebeurtenis_type": "BetalingVerwerkt",
  "gebeurtenis_kenmerk": "81029991234567891-7",
  "datumtijd_gebeurtenis": "2023-05-15T00:00:00+00:00",
  "datumtijd_verwerkt": "2023-05-15T00:00:00+00:00",
  "betalingskenmerk": "81029991234567891",
  "datumtijd_ontvangen": "2023-05-14T00:00:00+00:00",
  "ontvangen_door": "00000004000000044000",
  "verwerkt_door": "00000004000000044000",
  "bedrag": 16500
}
`),
		},
	}

	achterstanden := []systemModel.GebeurtenisKenmerk{}
	contactOpties := []systemModel.ContactOptie{}

	financieleZaak, err := model.NewFinancieleZaakFromDBEvents(dbEvents, achterstanden, contactOpties)

	assert.NoError(t, err)
	assert.NotNil(t, financieleZaak)
}

func TestNewFinancieleZaakFromDBEvents_ErrorCases(t *testing.T) {
	dbEvents := []repositories.DbEvent{
		{
			BaseEvent: systemModel.BaseEvent{
				GebeurtenisType: "FinancieleVerplichtingOpgelegd",
			},
			Data: nil,
		},
	}

	achterstanden := []systemModel.GebeurtenisKenmerk{}
	contactOpties := []systemModel.ContactOptie{}

	financieleZaak, err := model.NewFinancieleZaakFromDBEvents(dbEvents, achterstanden, contactOpties)

	assert.Error(t, err)
	assert.Nil(t, financieleZaak)
}
