// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/events"
)

type options struct {
	ListenAddress   string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the mock-source-system api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	OrganizationOin string `long:"organization-oin" env:"ORGANIZATION_OIN" description:"Organization oin"`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}
	eventRepository := repositories.NewEventStore(logger)
	financialClaimInformationUseCase := usecases.NewFinancialClaimInformationUseCase(logger, cliOptions.OrganizationOin, eventRepository)
	router := http_infra.NewRouter(financialClaimInformationUseCase, logger)

	event := events.DESS_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.DESS_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
